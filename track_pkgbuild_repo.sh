#!/bin/sh
if [[ $# -eq 0 ]] ; then
	echo 'must pass the name of a package'
	exit 1
fi
git submodule add ssh://aur@aur.archlinux.org/$1.git
