SUBMODULES=$(shell git submodule status | cut -f3 -d' ')

##
## Maintenance tasks
##

forgotrecursive: ## forgot to recursively clone? run this.
	git submodule update --init --recursive --remote

clean: ## remove downloaded/generated files
	rm -rf */*.zst
	rm -rf */{src,pkg}

	@# remove downloaded source files (TODO - yank this from PKGBUILDs)
	rm -rf capella-bin/capella-*-linux-gtk-*.tar.gz
	rm -rf capella-bin/*.png
	rm -rf cayley/cayley_*.tar.gz
	rm -rf cf-terraforming/cf-terraforming-*-amd64.tar.gz
	rm -rf clickup/*.AppImage
	rm -rf datasette/datasette-*.tar.gz
	rm -rf geoserver-bin/geoserver-*-bin.zip
	rm -rf haskell-hfuse/HFuse-*.tar.gz
	rm -rf julia-bin/julia-*.tar.gz
	rm -rf nanogui-git/nanogui/
	rm -rf partmc-git/partmc/
	rm -rf print_chords/print_chords-*.tar.gz
	rm -rf python-addict/python-addict-*.tar.gz
	rm -rf python-datauri/python-datauri-*.tar.gz
	rm -rf python-docstring-parser/docstring_parser-*.tar.gz
	rm -rf python-goes2go/goes2go-*.tar.gz
	rm -rf python-jsonargparse/jsonargparse-*.tar.gz
	rm -rf python-lightning-utilities/lightning_utilities-*.tar.gz
	rm -rf python-ompython/OMPython-*.tar.gz
	rm -rf python-ompython/osmc-pl-1.2.txt
	rm -rf python-open-clip-torch/v*.zip
	rm -rf python-overturemaps/overturemaps-*.tar.gz
	rm -rf python-playwright/playwright-python/
	rm -rf python-pyinstrument/pyinstrument-*.tar.gz
	rm -rf python-reqif/reqif-*.tar.gz
	rm -rf python-sgp4/sgp4-*.tar.gz
	rm -rf python-skyfield/skyfield-*.tar.gz
	rm -rf python-sphinx-needs/sphinx_needs-*.tar.gz
	rm -rf python-sphinx-data-viewer/sphinx_data_viewer-*.tar.gz
	rm -rf python-strictdoc/strictdoc-*.tar.gz
	rm -rf python-torchmetrics/torchmetrics-*.tar.gz
	rm -rf python-wagtail/wagtail-*.tar.gz
	rm -rf python-webdataset/webdataset-*.tar.gz
	rm -rf python-webio-jupyter-extension/webio_jupyter_extension-*.tar.gz
	rm -rf python-willow/willow-*.tar.gz
	rm -rf r-e1071/e1071_*.tar.gz
	rm -rf r-httr/httr_*.tar.gz
	rm -rf r-pbdzmq/pbdZMQ_*.tar.gz
	rm -rf r-rvest/rvest_*.tar.gz
	rm -rf r-selectr/selectr_*.tar.gz
	rm -rf r-uuid/uuid_*.tar.gz
	rm -rf roamer/roamer-*.tar.gz
	rm -rf spatialite-tools/spatialite-tools-*.tar.gz
	rm -rf tev/tev/
	rm -rf vifm-git/vifm/

	@# clean up misc files
	git -C geoserver-bin restore geoserver.service


##
## Other
##

################################################################################
# Help target
#
# from: <https://gist.github.com/prwhite/8168133?permalink_comment_id=4700889#gistcomment-4700889>
################################################################################
help:: ## show this help text
	@gawk -vG=$$(tput setaf 2) -vR=$$(tput sgr0) ' \
	  match($$0, "^(([^#:]*[^ :]) *:)?([^#]*)##([^#].+|)$$",a) { \
	    if (a[2] != "") { printf "    make %s%-18s%s %s\n", G, a[2], R, a[4]; next }\
	    if (a[3] == "") { print a[4]; next }\
	    printf "\n%-36s %s\n","",a[4]\
	  }' $(MAKEFILE_LIST)
	@echo -e "" # blank line at the end
.DEFAULT_GOAL := help

MAKEFLAGS += --no-builtin-rules --no-builtin-variables
