This is a set of Git repos for the AUR packages I maintain.
The key I use to access them is also included here (private key not checked in).

See: [List of my packages](https://aur.archlinux.org/packages?O=0&SeB=M&K=alhirzel&outdated=&SB=n&SO=a&PP=50&submit=Go), [My outdated packages](https://repology.org/projects/?maintainer=alhirzel@aur&inrepo=aur&outdated=1)


# Shortcuts

There are a few scripts in here which I use as shortcuts:

* `update_checksums.sh` - run to download files and output observed checksums
* `update_srcinfo.sh` - don't forget to do this before committing!
* `new_pkgbuild_repo.sh`
* `track_pkgbuild_repo.sh` - add a new submodule

